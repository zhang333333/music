import Vue from 'vue'
import VueRouter from 'vue-router'
import Home from '../App.vue'
import Main from '../views/Main.vue'
import MusicSheet from '../views/main/MusicSheet.vue'
import LocalMusic from '../views/main/LocalMusic.vue'
import FindMusic from '../views/main/FindMusic.vue'
import MusicSheetList from '../views/main/MusicSheetList.vue'
import SearchMusic from '../views/main/SearchMusic.vue'
import MusicAlbum from '../views/main/MusicAlbum.vue'
import MusicSinger from '../views/main/MusicSinger.vue'
import MusicLyric from '../views/main/MusicLyric.vue'

const originalPush = VueRouter.prototype.push
VueRouter.prototype.push = function push(localtion){
  return originalPush.call(this,localtion).catch(err=>err)
}
Vue.use(VueRouter)

  const routes = [
  {
    path: '/',
    name: 'Main',
    component: Main,
    children:[
      {
        path:'/sheetlist',
        name:'MusicSheetList',
        component:MusicSheetList
      },
      {
        path:'/sheet',
        name:'MusicSheet',
        component:MusicSheet
      },
      {
        path:'/local',
        name:'LocalMusic',
        component:LocalMusic
      },
      {
        path:'/find',
        name:'FindMusic',
        component:FindMusic
      },
      {
        path:'/search/:aaid',
        name:'SearchMusic',
        component:SearchMusic
      },
      {
        path:'/album',
        name:'MusicAlbum',
        component:MusicAlbum
      },
      {
        path:'/singer',
        name:'MusicSinger',
        component:MusicSinger
      },
      {
        path:'/lyric',
        name:'MusicLyric',
        component:MusicLyric
      },
    ]
  }
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
