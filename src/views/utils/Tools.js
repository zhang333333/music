// 格式化时长  秒数-----> 分钟：秒
exports.convertDuration = (time)=>{
    //计算分钟  单数返回‘01’  多位数‘010’
    const minutes = "0"+Math.floor(time/60)
    //计算秒数
    const seconds = "0"+Math.floor(time-minutes*60)
    return minutes.substr(-2)+":"+seconds.substr(-2)
}
// 格式化日期  时间戳 -----> 年-月-日
exports.formatDate = (time)=>{
    var t = new Date(time)
    return t.getFullYear() + "-" + (t.getMonth() + 1) + "-" +t.getDate()
}
