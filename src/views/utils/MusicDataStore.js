const Store = window.require('electron-store')
const path = require('path')
const uuid = require('uuid')
const nodeid3 = require('node-id3')
const fs = window.require('fs')

class DataStore extends Store{
    constructor(settings){
        super(settings)
        this.tracks = this.get('tracks')||[]
    }
    saveTracks(){
        this.set('tracks',this.tracks)
        return this
    }
    getTracks(){
        return this.get('tracks')||[]
    }
    addTracks(tracks){
        const tracksWithProps = tracks.map(track=>{
            let data = fs.readFileSync(track)
            let tags = nodeid3.read(data)
            return {
                mid:uuid.v1(),
                mName:(tags==undefined?"":tags.title)||path.basename(track.replace(/\\/g,'/'),'.mp3'),
                mPath:track,
                mSinger:tags==undefined?"": tags.artist,
                mAlbum:tags==undefined?"":tags.album,
                mSource: 'local'
            }
        }).filter(track=>{
            const currentTracksPath = this.getTracks().map(track=>track.mPath)
            return currentTracksPath.indexOf(track.mPath)<0
        })
        this.tracks = [...this.tracks,...tracksWithProps]
        return this.saveTracks()
    }
    deleteTrack(deletedId){
        this.tracks = this.tracks.filter(item=>item.mid !==deletedId)
        return this.saveTracks()
    }
}
module.exports = DataStore