import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)

export default new Vuex.Store({
    state:{
        currentMusic:{//正在播放的音乐
            mid:'',
        },
        playStatus:false,
        musicList:[],//歌曲播放列表
        musicListIndex:0,// musicList中当前播放音乐的下标
        searchText:'',//头部搜索框内容
    },
    mutations:{
        updateCurrentMusic(state, music) {
            state.currentMusic = music
        },
        updatePlayStatus(state, status){
            state.playStatus = status
        },
        updateMusicList(state, list) {
            state.musicList = list
        },
        updateMusicListIndex(state, v){
            state.musicListIndex = v
        },
        updateSearchText(state, v){
            state.searchText = v
        }
    }
})